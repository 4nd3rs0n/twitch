package twitch

import "net/http"

type StreamScheduleResp struct {
	Data       StreamScheduleInfo `json:"data"`
	Pagination Pagination         `json:"pagination"`
}

type StreamScheduleInfo struct {
	Segments []Segment `json:"segments"`
}

type Segment struct {
	ID               string     `json:"id"`
	StartTime        string     `json:"start_time"`
	EndTime          string     `json:"end_time"`
	Title            string     `json:"title"`
	CanceledUntil    string     `json:"canceled_until"`
	Category         []Category `json:"category"`
	IsRecurring      bool       `json:"is_recurring"`
	BroadcasterID    string     `json:"broadcaster_id"`
	BroadcasterName  string     `json:"broadcaster_name"`
	BroadcasterLogin string     `json:"broadcaster_login"`
	Vacation         []Vacation `json:"vacation"`
}
type Category struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}
type Vacation struct {
	StartTime string `json:"start_time"`
	EndTime   string `json:"end_time"`
}

type Pagination struct {
	Cursor string `json:"cursor"`
}

func GetStreamScheduleFromBroadcaster(client *http.Client, auth TwitchAuth, broadcasterID string, additionalParams []UrlParam) (StreamScheduleResp, error) {
	respBody := StreamScheduleResp{}

	req, err := http.NewRequest(http.MethodGet,
		"https://api.twitch.tv/helix/schedule", nil)
	if err != nil {
		return respBody, err
	}
	auth.Use(req)

	q := req.URL.Query()
	q.Add("broadcaster_id", broadcasterID)
	for _, param := range additionalParams {
		q.Add(param.Name, param.Value)
	}
	req.URL.RawQuery = q.Encode()

	err = must200DoReqAndParseBody(client, req, &respBody)
	return respBody, err
}

func GetStreamScheduleByIDs(client *http.Client, auth TwitchAuth, ids []string, additionalParams []UrlParam) (StreamScheduleResp, error) {
	respBody := StreamScheduleResp{}

	req, err := http.NewRequest(http.MethodGet,
		"https://api.twitch.tv/helix/schedule", nil)
	if err != nil {
		return respBody, err
	}
	auth.Use(req)

	q := req.URL.Query()
	for _, id := range ids {
		q.Add("id", id)
	}
	for _, param := range additionalParams {
		q.Add(param.Name, param.Value)
	}
	req.URL.RawQuery = q.Encode()

	err = must200DoReqAndParseBody(client, req, &respBody)

	return respBody, nil
}
