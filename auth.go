package twitch

import "net/http"

type TwitchAuth struct {
	Token    string
	ClientID string
}

func (a TwitchAuth) Use(req *http.Request) {
	req.Header.Set("Authorization", "Bearer "+a.Token)
	req.Header.Set("Client-Id", a.ClientID)
}
