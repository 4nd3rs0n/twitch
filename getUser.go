package twitch

import (
	"errors"
	"net/http"
)

var ErrNotFound = errors.New("Entity wan't found")

const (
	TYPE_ID    = iota
	TYPE_LOGIN = iota
)

type LoginOrID struct {
	Val string
	// Use TYPE_LOGIN or TYPE_ID here
	Type int
}

type GetUserResp struct {
	Data []UserInfo `json:"data"`
}
type UserInfo struct {
	ID              string `json:"id"`
	Login           string `json:"login"`
	DisplayName     string `json:"display_name"`
	Type            string `json:"type"`
	BroadcasterType string `json:"broadcaster_type"`
	Description     string `json:"description"`
	ProfileImgURL   string `json:"profile_image_url"`
	OfflineImgURL   string `json:"offline_image_url"`
	ViewCount       int    `json:"view_count"`
	Email           string `json:"email"`
	CreatedAt       string `json:"created_at"`
}

func GetUserByID(client *http.Client, auth TwitchAuth, id string) (UserInfo, error) {
	respBody := GetUserResp{}

	req, err := http.NewRequest(http.MethodGet,
		"https://api.twitch.tv/helix/users?id="+id, nil)
	if err != nil {
		return UserInfo{}, err
	}
	auth.Use(req)
	err = must200DoReqAndParseBody(client, req, &respBody)
	if err != nil {
		return UserInfo{}, err
	}
	if len(respBody.Data) < 1 {
		return UserInfo{}, ErrNotFound
	}
	return respBody.Data[0], err
}

func GetUserByLogin(client *http.Client, auth TwitchAuth, login string) (UserInfo, error) {
	respBody := GetUserResp{}

	req, err := http.NewRequest(http.MethodGet,
		"https://api.twitch.tv/helix/users?login="+login, nil)
	if err != nil {
		return UserInfo{}, err
	}
	auth.Use(req)
	err = must200DoReqAndParseBody(client, req, &respBody)
	if err != nil {
		return UserInfo{}, err
	}
	if len(respBody.Data) < 1 {
		return UserInfo{}, ErrNotFound
	}
	return respBody.Data[0], err
}

func GetUsers(client *http.Client, auth TwitchAuth, loginsAndIDs []LoginOrID) (GetUserResp, error) {
	respBody := GetUserResp{}

	req, err := http.NewRequest(http.MethodGet,
		"https://api.twitch.tv/helix/users", nil)
	if err != nil {
		return respBody, err
	}
	auth.Use(req)

	q := req.URL.Query()
	for _, loginOrID := range loginsAndIDs {
		switch loginOrID.Type {
		case TYPE_ID:
			q.Add("id", loginOrID.Val)
		case TYPE_LOGIN:
			q.Add("login", loginOrID.Val)
		}
	}
	req.URL.RawQuery = q.Encode()

	err = must200DoReqAndParseBody(client, req, &respBody)
	return respBody, err
}
