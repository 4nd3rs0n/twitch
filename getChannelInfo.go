package twitch

import (
	"net/http"
)

type ChannelInfoResp struct {
	Data []ChannelInfo `json:"data"`
}
type ChannelInfo struct {
	BroadcasterID               string   `json:"broadcaster_id"`
	BroadcasterLogin            string   `json:"broadcaster_login"`
	BroadcasterName             string   `json:"broadcaster_name"`
	BroadcasterLanguage         string   `json:"broadcaster_language"`
	GameID                      string   `json:"game_id"`
	GameName                    string   `json:"game_name"`
	Title                       string   `json:"title"`
	Delay                       int      `json:"delay"`
	Tags                        []string `json:"tags"`
	ContentClassificationLables []string `json:"content_classification_lables"`
	IsBrandedContent            bool     `json:"is_branded_content"`
}

func GetChannelInfo(client *http.Client, auth TwitchAuth, broadcasterID string) (ChannelInfo, error) {
	var respBody ChannelInfoResp

	req, err := http.NewRequest(http.MethodGet,
		"https://api.twitch.tv/helix/channels?broadcaster_id="+broadcasterID, nil)
	if err != nil {
		return ChannelInfo{}, err
	}
	auth.Use(req)
	err = must200DoReqAndParseBody(client, req, &respBody)
	if err != nil {
		return ChannelInfo{}, err
	}
	if len(respBody.Data) < 1 {
		return ChannelInfo{}, ErrNotFound
	}
	return respBody.Data[0], err
}

func GetChannelsInfo(client *http.Client, auth TwitchAuth, broadcasterIDs []string) (ChannelInfoResp, error) {
	var respBody ChannelInfoResp

	req, err := http.NewRequest(http.MethodGet,
		"https://api.twitch.tv/helix/channels", nil)
	if err != nil {
		return respBody, err
	}
	auth.Use(req)

	q := req.URL.Query()
	for _, id := range broadcasterIDs {
		q.Add("id", id)
	}
	req.URL.RawQuery = q.Encode()

	err = must200DoReqAndParseBody(client, req, &respBody)
	return respBody, err
}
