package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"gitlab.com/4nd3rs0n/twitch"
)

func main() {
	token := os.Getenv("TWITCH_TOKEN")
	clientID := os.Getenv("TWITCH_CLIENT_ID")
	userLogin := "4nd3rs0n_dev"

	client := &http.Client{}

	var auth = twitch.TwitchAuth{
		Token:    token,
		ClientID: clientID,
	}

	user, err := twitch.GetUserByLogin(client, auth, userLogin)
	if err != nil {
		log.Fatalf("Failed to get user's info: %s", err.Error())
	}
	fmt.Printf("%+v\n\n", user)
	channelInf, err := twitch.GetChannelInfo(client, auth, user.ID)
	if err != nil {
		log.Fatalf("Failed to get a channel info: %s", err.Error())
	}
	fmt.Printf("%+v\n\n", channelInf)

	schedule, err := twitch.GetStreamScheduleFromBroadcaster(client, auth, user.ID, []twitch.UrlParam{})
	fmt.Printf("%+v\n", schedule.Data)
}
