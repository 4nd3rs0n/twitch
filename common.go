package twitch

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

type UrlParam struct {
	Name  string
	Value string
}

func must200DoReqAndParseBody(client *http.Client, request *http.Request, bodyPtr any) error {
	resp, err := client.Do(request)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		msg, err := io.ReadAll(resp.Body)
		if err != nil {
			panic(err.Error())
		}
		return fmt.Errorf("Response code isn't 200. Code: %d. Body: %s", resp.StatusCode, string(msg))
	}
	return json.NewDecoder(resp.Body).Decode(bodyPtr)
}
